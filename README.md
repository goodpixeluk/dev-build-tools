# Ollie's Build Tools

Webpack & Gulp build tools for simple web projects.

## Installation

Use the NPM package manager to install gulp, webpack and any dependancies.

```bash
npm install
```

## Usage

Simple run the gulp command to start watching for changes.

```bash
gulp
```